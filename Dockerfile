FROM python:3
ADD index.html index.html
ADD import.py import.py
EXPOSE 80
ENTRYPOINT [“python3”, import.py”]